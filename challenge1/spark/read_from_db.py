from typing import Dict

from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from utils import CONN_PROPERTIES, JDBC_URL, get_spark_session


def read_from_db(
    spark: SparkSession, jdbc_url: str, con_properties: Dict[str, str], query: str
) -> DataFrame:
    return spark.read.jdbc(
        url=jdbc_url, table=f"({query}) AS t", properties=con_properties
    )


if __name__ == "__main__":
    ss = get_spark_session()

    # Create a query
    query_airlines_table = """
                SELECT * FROM airlines
            """

    # run the query
    df = read_from_db(ss, JDBC_URL, CONN_PROPERTIES, f"{query_airlines_table}")

    df.show(truncate=100)
    print(df.count())
    df.printSchema()

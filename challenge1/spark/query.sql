SELECT CONCAT(
	LEFT(LPAD(CAST(sched_dep_time AS VARCHAR), 4, '0'), 2),
	':',
	RIGHT(LPAD(CAST(sched_dep_time AS VARCHAR), 4, '0'), 2)
	)
FROM public.flights

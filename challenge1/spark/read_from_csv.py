from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import StringType, StructField, StructType

from utils import get_spark_session


def read_csv(spark: SparkSession, csv_path: str, delimiter: str = ",", header: bool = True, schema = None) -> DataFrame:
    if schema:
        return spark \
            .read \
            .schema(schema) \
            .options(delimeter=delimiter) \
            .csv(csv_path, header=header)

    else:
        return spark \
            .read \
            .options(delimeter=delimiter) \
            .csv(csv_path, header=header)

def get_airlines_data(spark: SparkSession) -> DataFrame:
    airlines_schema = StructType(
        [
            StructField("carrier", StringType(), True),
            StructField("name", StringType(), True),
        ]
    )

    return (
        spark.read.schema(airlines_schema)
        .options(delimeter=",")
        .csv("../dataset/nyc_airlines.csv", header=True)
    )


if __name__ == "__main__":
    get_airlines_data(get_spark_session()).show()

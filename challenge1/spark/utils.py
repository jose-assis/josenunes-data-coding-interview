from typing import Dict

from pyspark.sql import SparkSession

JDBC_URL: str = "jdbc:postgresql://localhost:5432/dw_flights"
CONN_PROPERTIES: Dict[str, str] = {
    "user": "postgres",
    "password": "Password1234**",
    "driver": "org.postgresql.Driver",
}


def get_spark_session() -> SparkSession:
    """creates a spark session object"""
    return (
        SparkSession.builder.appName("my-app")
        .config("spark.jars.packages", "org.postgresql:postgresql:42.6.0")
        .getOrCreate()
    )

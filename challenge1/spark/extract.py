from utils import get_spark_session, CONN_PROPERTIES, JDBC_URL
from write_to_db import write_to_db
from read_from_csv import read_csv

from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import StringType, StructField, StructType, DoubleType, IntegerType, TimestampType


def extract_airports(spark):
    airports_schema = StructType(
        [
            StructField("faa", StringType(), True),
            StructField("name", StringType(), False),
            StructField("latitude", DoubleType(), False),
            StructField("longitude", DoubleType(), False),
            StructField("altitude", DoubleType(), False),
            StructField("timezone", IntegerType(), False),
            StructField("dst", StringType(), False),
            StructField("timezone_name", StringType(), False),
        ]
    )
    return read_csv(spark, "/workspaces/josenunes-data-coding-interview/challenge1/dataset/nyc_airports.csv", schema=airports_schema)

def extract_flights(spark):
    flights_schema = StructType(
        [
            StructField("_d0", StringType(), True),
            StructField("year", IntegerType(), False),
            StructField("month", IntegerType(), False),
            StructField("day", IntegerType(), False),
            StructField("dep_time", IntegerType(), True),
            StructField("sched_dep_time", IntegerType(), True),
            StructField("dep_delay", IntegerType(), True),
            StructField("arr_time", IntegerType(), True),
            StructField("sched_arr_time", IntegerType(), True),
            StructField("arr_delay", IntegerType(), False),
            StructField("carrier", StringType(), True),
            StructField("flight", IntegerType(), False),
            StructField("tailnum", StringType(), False),
            StructField("origin", StringType(), False),
            StructField("dest", StringType(), False),
            StructField("air_time", DoubleType(), False),
            StructField("distance", DoubleType(), False),
            StructField("hour", IntegerType(), False),
            StructField("minute", IntegerType(), False),
            StructField("time_hour", TimestampType(), False),
        ]
    )
    df = read_csv(spark, "/workspaces/josenunes-data-coding-interview/challenge1/dataset/nyc_flights.csv", schema=flights_schema)
    return df.drop("_d0")

if __name__ == "__main__":
    # spark = get_spark_session()
    # airports_df = extract_airports(spark)

    # write_to_db(airports_df, JDBC_URL, CONN_PROPERTIES, "airports")

    spark = get_spark_session()
    flights_df = extract_flights(spark)

    write_to_db(flights_df, JDBC_URL, CONN_PROPERTIES, "flights")

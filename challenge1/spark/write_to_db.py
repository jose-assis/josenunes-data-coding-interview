from typing import Dict

from pyspark.sql.dataframe import DataFrame
from read_from_csv import get_airlines_data
from utils import CONN_PROPERTIES, JDBC_URL, get_spark_session


def write_to_db(
    df: DataFrame,
    jdbc_url: str,
    con_properties: Dict[str, str],
    table: str,
    mode: str = "overwrite",
):
    df.write.mode(mode).jdbc(url=jdbc_url, table=table, properties=con_properties)


if __name__ == "__main__":
    spark = get_spark_session()

    airlines_df = get_airlines_data(spark)

    write_to_db(airlines_df, JDBC_URL, CONN_PROPERTIES, "airlines")
